# -*- mode: python -*-

from PyInstaller.building.build_main import Analysis
import sys
from frkl_pkg.utils import pyinstaller_helpers


block_cipher = None

# remove tkinter dependency ( https://github.com/pyinstaller/pyinstaller/wiki/Recipe-remove-tkinter-tcl )
sys.modules["FixTk"] = None

extra_datas = [
  ('dist/linux-gnu/ansible.pex', '.'),
  ('wrapper_scripts/ansible-playbook', '.'),
]

analysis_args = pyinstaller_helpers.get_analysis_args(block_cipher=block_cipher, extra_datas=extra_datas)
a = a = Analysis(**analysis_args)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    [],
    exclude_binaries=True,
    name="freckles-pkg",
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    console=True,
)
coll = COLLECT(
    exe, a.binaries - TOC([('libtinfo.so.5', None, None)]), a.zipfiles, a.datas, strip=False, upx=True, name="freckles-pkg"
)

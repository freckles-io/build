# -*- mode: python -*-

from PyInstaller.building.build_main import Analysis
import platform
import sys
from frkl_pkg.utils import pyinstaller_helpers


block_cipher = None

# remove tkinter dependency ( https://github.com/pyinstaller/pyinstaller/wiki/Recipe-remove-tkinter-tcl )
sys.modules["FixTk"] = None


if platform.system() == "Linux":
    os_type = "linux-gnu"
else:
    os_type = "darwin15"


extra_datas = []
#  ('dist/{}/ansible_cp2.7.pex'.format(os_type), '.'),
#  ('wrapper_scripts/ansible-playbook', '.'),
#  ('wrapper_scripts/ansible-galaxy', '.'),
#]

analysis_args = pyinstaller_helpers.get_analysis_args(block_cipher=block_cipher, extra_datas=extra_datas)
a = a = Analysis(**analysis_args)
pyz = PYZ(a.pure, a.zipped_data, cipher=block_cipher)

exe = EXE(
    pyz,
    a.scripts,
    a.binaries - TOC([('libtinfo.so.5', None, None)]),
    a.zipfiles,
    a.datas,
    [],
    name="freckles",
    debug=False,
    bootloader_ignore_signals=False,
    strip=False,
    upx=True,
    runtime_tmpdir=None,
    console=True,
)

#!/usr/bin/env bash

set -e
set -x

function build_pkg () {

    venv_path=${1}
    spec_file=${2}
    target=${3}

    echo "building package"

    source "${venv_path}/bin/activate"

    pyinstaller --clean -y --dist "${target}" --workpath /tmp "${spec_file}"

    deactivate

    echo "  -> package built"

}

function build_ansible_pex () {

    venv_path=${1}
    target=${2}
    force=${3}

    if [ -f ${target} ]; then
        return 0
    fi

    echo "building ansible pex file: ${target}"

    install_python_version "2.7.16"
#    install_python_version "3.5.7"
#    install_python_version "3.6.8"

    source "${venv_path}/bin/activate"

    pex --python-shebang="/usr/bin/env python" \
        --python ~/.pyenv/versions/2.7.16/bin/python \
        -o ${target} ansible==2.7.10 jmespath pywinrm

#        --python ~/.pyenv/versions/3.5.7/bin/python \
#        --python ~/.pyenv/versions/3.6.8/bin/python \

#    pex -o ${target} ansible==2.7.10 jmespath pywinrm

    deactivate

    echo " -> ansible.pex build finished"

}

function create_venv () {

    venv_path=${1}
    python_version=${2}

    echo "creating virtualenv"

    ~/.pyenv/versions/${python_version}/bin/python -m venv "${venv_path}"

    echo " -> venv created"

}

function install_dependencies () {

    venv_path=${1}
    source "${venv_path}/bin/activate"

    echo "installing dependencies"

    pip install -r "${THIS_DIR}/requirements_pkg.txt" -c "${THIS_DIR}/constrain.txt"
    pip uninstall -y enum34

    deactivate

    echo " --> dependencies installed"

}

function install_python_version () {

    python_version=${1}

    env PYTHON_CONFIGURE_OPTS="--enable-shared" pyenv install -s ${python_version}

}
function build () {

    echo "starting build"

    venv_path="${THIS_DIR}/venv_build"
#    rm -rf "${venv_path}"
    python_version="3.6.8"

    install_python_version "${python_version}"

    create_venv "${venv_path}" "${python_version}"
    install_dependencies "${venv_path}"

#    build_ansible_pex "${venv_path}" "${THIS_DIR}/dist/${OSTYPE}/ansible_cp2.7.pex"

    build_pkg "${venv_path}" "${THIS_DIR}/freckles-onefile.spec" "${THIS_DIR}/dist/${OSTYPE}"
#    build_pkg "${venv_path}" "${THIS_DIR}/freckles-folder.spec" "${THIS_DIR}/dist/${OSTYPE}"

    cp "${THIS_DIR}/dist/${OSTYPE}/freckles" "${THIS_DIR}/dist/${OSTYPE}/frecklecute"
    rm -rf ${THIS_DIR}/{build,generated}

    echo "  -> build finished"

}

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${THIS_DIR}

rm -rf ${THIS_DIR}/{build,generated}

if [ -z "DOCKER_BUILD" ]; then
    DOCKER_BUILD=false
fi

if [ -f /.dockerenv ]; then

    build
    chown -R --reference=. "${THIS_DIR}/venv_build"
    chown -R --reference=. "${THIS_DIR}/dist"


else

    if [ "$DOCKER_BUILD" = true ]; then

        docker run -v "${THIS_DIR}:/src/" frkl/py-builder

    else

        build

    fi


fi

